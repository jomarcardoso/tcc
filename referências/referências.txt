Maujor

Patterns help you split a problem into Lego-like blocks and focus on the unique parts
of the problem while abstracting out a lot of �been there, done that, got the T-shirt�
kind of details.

Javascript P\atterns xiii

In software development, a pattern is a solution to a common problem. A pattern is
not necessarily a code solution ready for copy-and-paste but more of a best practice, a
useful abstraction, and a template for solving categories of problems. pg 1

Single var Pattern
Using a single var statement at the top of your functions is a useful pattern to adopt. It
has the following benefits:
� Provides a single place to look for all the local variables needed by the function
� Prevents logical errors when a variable is used before it�s defined (see �Hoisting: A
Problem with Scattered vars� on page 14)
� Helps you remember to declare variables and therefore minimize globals
� Is less code (to type and to transfer over the wire)


resolver logo o bug para n�o ter que procurar e entender ele de novo depois.

 It�s best if you can fix a bug right away, as
soon you find it; this is when the problem your code solves is still fresh in your head. pg 9

As a result of the changes, the few man-hours spent writing the code initially end up
in man-weeks spent reading it. That�s why creating maintainable code is critical to the
success of an application.
Maintainable code means code that:
� Is readable
� Is consistent
� Is predictable
� Looks as if it was written by the same person
� Is documented



Pattern Language livro
http://www.arch.mcgill.ca/prof/mellin/articles/patternla.pdf
http://srvd.grupoa.com.br/uploads/imagensExtra/legado/A/ALEXANDER_Christopher/Linguagem_Padroes/Lib/Capitulo_01.pdf
http://butunclebob.com/ArticleS.UncleBob.PrinciplesOfOod

sobre excesso de padr�es
http://wiki.c2.com/?AreDesignPatternsMissingLanguageFeatures
http://www.paulgraham.com/icad.html
http://wiki.c2.com/?DesignPatternsInDynamicProgramming
http://norvig.com/design-patterns/ppframe.htm

Object-Oriented JavaScript by yours truly (Packt Publishing)
� JavaScript: The Definitive Guide by David Flanagan (O�Reilly)
� JavaScript: The Good Parts by Douglas Crockford (O�Reilly)
� Pro JavaScript Design Patterns by Ross Hermes and Dustin Diaz (Apress)
� High Performance JavaScript by Nicholas Zakas (O�Reilly)
� Professional JavaScript for Web Developers by Nicholas Zakas (Wrox)


