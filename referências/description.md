#### Uma linguagem de padrões XIV

*"Cada padrão descreve um problema que ocorre repetidas vezes em nosso meio ambiente e então descreve o ponto central da solução do problema, de modo que você possa usar a mesma solução milhares de vezes, mas sem jamais ter de repeti-la."*

#### Uma linguagem de padrões XVI

*"Em  suma,  nenhum  padrão  é  uma  entidade  isolada.  Cada  padrão existe somente porque é sustentado por outros padrões: os padrões maiores, dentro dos quais ele se inclui, os padrões do mesmo tamanho, que o circundam, e os padrões menores, nele inserido."*

*"Esta é uma visão fundamental do mundo. Isso significa que quando você constrói uma coisa não pode meramente construí-la de forma isolada, mas deve consertar o mundo ao seu redor assim como seu interior, de modo que o mundo ao qual aquele lugar pertence se torne mais coerente, mais completo, e que aquilo que você fizer assuma seu lugar na rede da natureza, enquanto você a faz."*

#### Uma linguagem de padrões XVII

*"Cada solução é exposta de modo que ela apresente o campo essencial de relacionamentos necessários para resolver o proble-ma, mas de uma maneira muito geral e abstrata – assim você poderá resolver o problema por si só, da sua própria maneira, adaptando-o às suas preferências e às condições específicas do local em que você está trabalhando."*

*"Por  esse  motivo,  tentamos  redigir  cada  solução  de  maneira  que ela não lhe imponha algo. A solução contém apenas aqueles pontos fundamentais que não podem ser deixados de lado se você realmente quer resolver o problema. "*