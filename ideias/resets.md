# Resets

Além do normalize as seguintes configurações:

```css
html {
  box-sizing: border-box;
  height: 100%;
  font-family: sans-serif;
}

*,
*::before,
*::after {
  box-sizing: inherit;
}

body {
  min-height: 100%;
}

:focus {
	outline: 0;
}

h1,
h2,
h3,
h4,
h5,
h6,
p,
pre,
ul,
dl {  
  margin-top: 0;
  margin-bottom: 0;
}

/* remove as aspas, não sei a necessidade */
blockquote, q {
	quotes: "" "";
}

blockquote:before,
blockquote:after,
q:before,
q:after {
	content: '';
	content: none;
}

body,
blockquote,
figure {
  margin: 0;
}

img {
  max-width: 100%;
  vertical-align: middle;
}

a {
  color: inherit;
  text-decoration: none;
}

ol,
ul {
  padding-left: 0;
  list-style-type: none;
}

/*
* o padding do botão é útil
*/
button,
input[type="button"] {
  border: 0;
}

/*
 * padding e border ficam para serem sobrescritos pelo estilo
 */
fieldset {
  margin-left: 0;
  margin-right: 0;
}

table {
	border-collapse: collapse;
	border-spacing: 0;
}

th {
  font-weight: normal;
  text-align: left;
}

dd {
  margin-left: 0;
}

textarea {
  vertical-align: top;
  resize: vertical;
}
```

## Referências

> http://meyerweb.com/eric/thoughts/2007/05/01/reset-reloaded/

> http://www.christianmontoya.com/2007/02/01/css-techniques-i-use-all-the-time/

> https://gist.github.com/DavidWells/18e73022e723037a50d6