## Padrões de projeto frontend

Construção seria os materiais e o processo usado para construir algo. Um padrão de construção seria o a proporção de cimento, areia e água.

Arquitetura seria a organização de nossa construção. Um padrão de arquitetura seria as dimensões de uma churrasqueira. Esses padrões são compostos por outros menores e devem contextualizar outros padrões maiores (A Pattern Language XVI), os padrões de construção seriam os padrões menores.

### Padrões estruturais Javascript

#### Usar destructuring e alias

Em javascript podemos usufruir o máximo desses dois para deixar o nosso código mais limpo. Vantagens:
- Não precisa dizer o nome do atributo de um objeto, ```{ value: data.value }``` e sim ```{ value }```.
- Não precisa acessar objectos com "." se vai usar apenas os atributos deste
  
Alguns exmplos:
```JAVASCRIPT
// errado
arg => ({ argAttr: arg.attr })
// correto
({ attr }) => ({ attr })
// se o atributo não tem o nome que desejas
({ attr: wanted }) => ({ wanted })
```
O exemplo abaixo mostra como podemos renomear uma argumento para modificá-lo:
```JAVASCRIPT
function ({ algumaCoisa: algumasCoisas, index }) {
  const algumaCoisa = algumasCoisas[index];

  façaAlgo({ algumaCoisa });
}
```
da forma apresentada nos exemplos nenhuma vez precisamos declarar um objeto com seu nome + atributo.

#### Todo listener e toda função assíncrona deve chamar uma função de callback. 

Ao criar um código que seja reaproveitável quem for usar pode precisar que uma tarefa seja executada depois

#### Props para todos os filhos de um componente do tipo molécula

Os componentes moléculas assim como os átomos podem ser usados em toda a aplicação, mas são diferentes dos átomos por precisarem renderizar mais nodes, e para aumentar o controle desses é importante enviar as propriedades de cada um. Exemplo:
```JAVASCRIPT
function Component({ node1, node2, ...props }) {
    return (
        <div {...props}>
            <div {...node1}/>
            <div {...node2}/>
        </div>
    )
}
```
Dessa forma aumenta o controle do componente.

#### os parâmetros não obrigatórios serem dentro de um objeto (options)

vantagens dessa abordagem:

- não precisa enviar parâmetros nulos para não quebrar a sequência, exemplo funçãoChamada('param', null, 'param') aqui precisava enviar o terceiro, mas não o segundo.
- fica visível os parâmetros obrigatórios fora do objeto.

#### composição

Preciso estudar isso, mas com composição não precisa sobrescrever quem você está extendendo nem adivinhar o que está recebendo dele.

#### imutabilidade

Javascript por ser uma linguagem híbrida OO e funcional permite que os valores sejam mudados, isso não segue o padrão funcional e se quebrado a regra problemas como alteração de valores em lugares indevidos podem acontecer. Não alterar o valor em uma função e sim retornar num novo.

```js
function({ ...value }) {
  setState(value);
}
```

#### funções puras

São funções sem side effect, ou seja não afetam e não são afetadas pelo exterior. A exceção dessa regra é quando estamos trabalhando dentro de outra função aproveitando de seus valores (closure).

Ver Sobre efeitos colaterais no clean code

#### closures

O uso de closures para proteger membros de uma função ou para reaproveitar valores. Exemplo

```JAVASCRIPT
function dataBase() {
  const data = {
    /* ... */
  }

  function getDataByName(name) {
    return data[name];
  }
  return getDataByName;
}
``` 

#### If sem else

O if não precisa ser seguido de else, e há varias formas de evitar:

- inicializar variáveis (mencionado abaixo)
- return no if evita executar o outro código que seria do else
- ...

#### return primeiro

Ao invés de adicionar a complexidade e aumentar o aninhamento do código dentro de um if, há casos que pode-se apenas fazer um if com return. Exemplo:
```JAVASCRIPT
// errado
function test() {
    if (canTest) {
        /* ... */
    }
}

// certo
function test() {
    if (!canTest) return
    /* ... */
}
```

#### Declarar variáveis no topo

Javascript Patterns 31
Segungo [w3Schools](https://www.w3schools.com/js/js_best_practices.asp)
- Give cleaner code
- Provide a single place to look for local variables
- Make it easier to avoid unwanted (implied) global variables
- Reduce the possibility of unwanted re-declarations

#### Inicializar variáveis

Segungo [w3Schools](https://www.w3schools.com/js/js_best_practices.asp)
- Give cleaner code
- Provide a single place to initialize variables
- Avoid undefined values

Pode ser usado também nos argumentos de uma função.
Ver também em clean code

Um bom uso é usar como valor padrão, por exemplo:
```JAVASCRIPT
let message = 'tudo certo';
if (somethingWrong) message = 'nada certo';
```
Isso evita o verboso if else

#### valor padrão para os parâmetros

Quando pode ser usado:
- se na maioria dos casos será esse valor
- quando não há precisão no valor, qualquer um da um resultado sem dar erro, como por exemplo um tamanho

Quando não usar:
- quando é preciso que o usuário do componente saiba que ele não informou.

#### Eventos sempre chamarem os argumentos para propagar

Pode ser que quem esteja chamando uma função também preciso do evento para fazer mais alguma coisa, logo sempre que receber uma variável do tipo "onAlgumaCoisa" se o evento "algumaCoisa" for disparado esta deva ser chamada.
```JAVASCRIPT
const onClick = () => alert('clicou');

function teste(onClick) {
    const element = document.querySelector('some-element');
    function handleClick(event) {
        if(onClick) onClick(event);
        /* faz outras coisas */
    }

    element.addEventListener('click', handleClick);   
}

teste(onClick)
```
Assim apenas um preciso cuidar do evento e várias podem usufruir.

#### seletor no js nunca ser classe 

Isso vai de encontro com que classes são a "classificação" daquele elemento, e podem ser mudadas para atender o CSS. Usar data-js ou data-component.

#### [Funções devem fazer uma coisa](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Retirado do clean code.

#### [Extender ao invés de modificar / Princípio do Aberto/Fechado (OCP)](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver no clean code. Também pode ser substituído por composição. Ver também SOLID.

```JAVASCRIPT
class SuperArray extends Array {
  diff(comparisonArray) {
    const hash = new Set(comparisonArray);
    return this.filter(elem => !hash.has(elem));
  }
}
```

#### [Princípio da Segregação de Interface (ISP)](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver SOLID. O princípio se refere a classes, mas trazendo para o Javascript não criar módulos com muita coisa que não será usado e sim quebrar para várias necessidades.

#### [Princípio de Substituição de Liskov (LSP)](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver SOLID. Se quem está extendo ou sendo composto não é totalmente válido para o outro não deve ser usado.

#### [Princípio da Inversão de Dependência (DIP)](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver SOLID.

- Módulos de alto nível não devem depender de módulos de baixo nível. Ambos devem depender de abstrações.
- Abstrações não devem depender de detalhes. Detalhes devem depender de abstrações.

#### [Apenas um conceito por teste](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Não fazer testes múltiplos, se nao, não saberá onde ocorreu o erro.

#### seletores css e js devem sempre buscar dentro da raíz do componente 

Isso evita o trabalho de vasculhar toda a DOM e ainda de encontrar um elemento que não queira.
```elComponent.querySelector``` ao invés de ```document.querySelector```.

#### [Encapsular condicionais](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Isso da nome às condicionais. Ver clean code.

```JAVASCRIPT
function shouldShowSpinner(fsm, listNode) {
  return fsm.state === 'fetching' && isEmpty(listNode);
}
```

#### [Evite condicionais](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Retirado do clean code. Muitas vezes o uso de condicionais segnifica que a função está fazendo mais do que deveria.

#### [Getters e Setters](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver clean code. Se usar os setters pode-se validar o valor antes, se usar os getters pode-se fazer o tratamentos antes. No caso de não usar depois pode ser preciso refatorar caso precise. No caso do javascript pode-se usar o getter e o setter sem refatorar, então não é regra.

#### [Membros privados](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver clean code. Isso evita de usar o que não deve ser usado, por exemplo proteger um atributo que deve passar por um getter ou um setter.

#### [Quando possível fazer retornos para encadear](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver clean code. Isso significa, os métodos retornarem um valor para que o próximo aproveite. Exemplo:
```JAVASCRIPT
'string-teste'.toUpperCase().split('').join(' ');
```

#### Async await no lugar de promises

Muito mais limpo.

#### [Não ignorar erros](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver clean code. Se um erro aconteceu ele deve ser tratado e não feito um console...

#### [Princípio da Responsabilidade Única (SRP)](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver Solid. Uma classe (ou função) não ter mais do que um motivo para mudar.

#### [Evitar otimizar](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Ver clean code. Motivos:

- não precisa reduzir linhas de código se no final a diferença será insignificante comparado a uma imagem ou um video.
- após carregada a página não importa o quanto custa fazer alguma coisa, pois até mesmo um acesso a DOM é muito barato.
- deixe a redução de variáveis para otimizadores, o código tem que ser claro.

#### comentar o código

Ver JS Patterns pg. 30. Obviamente após ter escrito um trecho de código seguindo os padrões do clean code se ainda for difícil de entender é válido usar comentários para complementar, pois estes são mais práticos que escrever uma documentação. Não é problema escrever mais comentários do que códigos se isso vai ajudar a não atrasar o desenvolvimento. "The most important habit, yet hardest to follow, is to keep the comments up to date, because outdated comments can mislead and be much
worse than no comments at all." Javascript Patterns 30.

#### Documentação embutida no código

A ideia é remover a tarefa entediante de escrever uma documentação e fazer ela auto gerada através dos comentários feitos com "the JSDoc Toolkit (http://code.google.com/p/jsdoc-toolkit/)
and YUIDoc (http://yuilibrary.com/projects/yuidoc)." Javascript Patter 30. Usar uma ferramenta para gerar a documentação.

### Padrões de design Javascript

#### Index em toda pasta JS

Toda pasta ter o se index.js exportando o que há na pasta, além de seguir alguma convensão (buscar) tem as seguintes vantagens e desvantagens:

- Mover as pastas ou renomeá-las não tem que buscar os arquivos até a pasta onde estão e sim buscar pelo index da sua estrutura.
- O index que exporta o componente evita do componente se chamar index, logo a aplicação inteira se chama index...
- Aumenta o trabalho de importar, pois tens de criar a linha de imoprtação e exportação em vários index.
- torna as importações mais próximas, pois precisa chegar no index e não no componente específico

#### eventos se chamarem onAlgumaCoisa

Atualmente é usado isso em "onClick, onChange...", além disso qualquer outro evento que não seja de manipulação da DOM sejam com esses nomes, exemplo "onError, onSuccess".

#### manipuladores de eventos se chamam handleAlgumaCoisa

Se estiver com dúvida por que não chamar o manipulador do evento de onAlgumaCoisa também, segue o exemplo:
```JAVASCRIPT
const onClick = () => alert('clicou');

function teste(onClick) {
    const element = document.querySelector('some-element');
    function handleClick(event) {
        onClick(event);
        /* faz outras coisas */
    }

    element.addEventListener('click', handleClick);   
}

teste(onClick)
```
assim quem chamou a funcão de teste também espera um clique e fica notável isso.

#### nomenclatura de elementos DOM com elAlgumaCoisa

Padrão para perceber que se trata de um elemento da DOM, assim como era usado em jQuery $AlgumaCoisa

#### [Nomes de variáveis e funções com significado](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Retirado do livro clean code (buscar), There are only two hard things in Computer Science: cache invalidation and naming things. Phil Karlton. Exemplos:
- errado: ```const yyyymmdstr = moment().format('YYYY/MM/DD');```
- correto: ```const currentDate = moment().format('YYYY/MM/DD');```

#### [Nomes pesquisáveis  e parâmetros explicativos](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Retirado do livro clean code:
- errado: ```setTimeout(blastOff, 86400000);```
- correto: ```setTimeout(blastOff, MILLISECONDS_IN_A_DAY);```

#### [Não repetir contextos](https://medium.com/trainingcenter/c%C3%B3digo-limpo-vers%C3%A3o-javascript-80adecafdbec)

Retirado do livro clean code:
- errado: 
```JAVASCRIPT
const Car = {
  make: 'Honda',
  model: 'Accord',
  color: 'Blue'
};
```
- correto: ```setTimeout(blastOff, MILLISECONDS_IN_A_DAY);```

#### Outros padrões de nomenclatura

- constantes em letras maiúsculas ```DAYS_OF_WEEK```
- classes em Pascal Case ```BlueWhale```
- funções iniciarem com verbo ```eraseDataBase```
- funções que retornam um booleano ```isDate```

#### Proibido letras maiúsculas em arquivos

Evita conflitos de git e sistemas operacionais sem case sensitive.

#### Nome do arquivo ter o seu tipo

Fácil de indentificar do que se trata. Exemplos:
- alguma-coisa.component.js
- alguma-coisa.route.js

#### Prefixar arquivos e componentes que tenham características da aplicação

Facilmente identificável. Ver sobre componentes agnósticos e da aplicação. Exemplo o arquivo arz-input.component.js exportar ArzInput.

#### Componentes agnósticos

A base de um componente ser um componentes agnóstico, sem estilo ou qualquer coisa de um projeto. Isso torna o componentes facilmente reutilizável.

#### Componentes controlados

Estes são classificados como sem "reação" apenas fazem processos e retornam o que foi pedido, eles não alteram nada de dentro deles. Ler no README.

#### Componentes reativos

Estes que chamam os componentes controlados e reagem a eles. Ler no README.

#### Componentes de serviços

Estes não precisam de outros em sua composição, eles entregam pronto o que foi pedido. Ver README.

#### [Atomic desing](http://bradfrost.com/blog/post/atomic-web-design/)

O que já usei:

- componentes dentro da pasta atom com sufixo .atom são Input, Form, Button...

### Padrões de CSS

#### Media query Central

Atualmente a maior parte das mudanças de CSS estão entre a versão mobile e a desktop, então ao invés de setar valores e uma e sobrescrever em outra melhor seria fazer um ponto central para não ter de resetar CSS que nunca foi necessário naquela versão.

```CSS
button {
  background: blue;
  margin-bottom: 15px;
}

@media (min-width: 769px) {
  button {
    margin-bottom: 0;
    
  }
}
```

Ao invés de resetar...

```CSS
button {
  background: blue;
}

@media (max-width: 768px) {
  button {
    margin-bottom: 15px;    
  }
}

@media (max-width: 769px) {
  button {
    border: 2px solid black;
  }
}
```
dessa forma facilita a renderização e não precisa se preocupar com propriedades que foram colocados e não serão usadas, normalmente apenas precisa se preocupar com sobrescritas comuns e não resets.

#### Classes com seletor mais forte primeiro

Assim mantém semanticamente o padrão e denota prioridade.

```html
<!-- HTML do tooltip -->
<div class="Tooltip">
    <h3 class="Tooltip-title Heading3">TITLE</h3>
    <p class="Tolltip-message Paragraph">message</p>
</div>
```

#### Suit CSS

O melhor padrão que já encontrei.

### Padrões de versionamento

#### [Versionamento semântico](https://semver.org/)

Ler sobre.

### Boas práticas de desenvolvimento

#### Revisitar o código

Revisitar o código, sendo para editar ou para documentar.
"The same applies to writing code. When you sit down and solve a problem, that solution
is merely a first draft. It produces the desired output, but does it do so in the best way?
Is it easy to read, understand, maintain, and update? When you revisit your code, ideally
34 | Chapter 2: Essentials
after some time has passed, you will most certainly see areas for improvement—ways
to make the code easier to follow, remove some inefficiencies, and so on" Javascript Patterns 34-35

#### Peer Review

Chamar o colega e ver se ele entende o teu código.
"Again, as with writing API docs or any type of documentation, peer reviews help you
write clearer code, simply because you know someone else will need to read and understand what you’re doing."