# Componentes HTML e CSS

Um componente da DOM é de estilo, apenas com HTML e CSS.

```HTML
<button>Botão</button>
```

Sabemos que os navegadores colocam propriedades CSS iniciais então este "button". Qualquer elemento é um componente com estilo padrão.

Agora podemos ter componentes com o estilo que desejarmos, algumas bibliotecas podem facilitar isso trazendo vários componentes prontos.

Para classificarmos um componente podemos usar por exemplo o "Atomic Design" para organizar os componentes conforme o seu papel no parte visual.

## Formas de criar componentes

Três formas de criar o componente Tooltip. É importante a escolha de um deses por projeto para que hava um padrão maior.

### Componentes curtos

```scss
// CSS pré definido
.Heading3 {
    ...
}

.Paragraph {
    ...
}
```

```html
<!-- HTML do tooltip -->
<div class="Tooltip">
    <h3 class="Heading3">TITLE</h3>
    <p class="Paragraph">message</p>
</div>
```

```scss
// CSS do tooltip com possíveis sobrescritas
.Tooltip {
    ...
}

.Tooltip .Heading3 {
    ...
}

.Tooltip .Paragraph {
    ...
}
```

#### Vantagens:

- prático, alguns componentes têm só uma classe.
- menos código.

#### Desvantagens

- nem sempre a classe descreve exatamente o que o elemento significa naquele contexto.
- seletores fortes.
- pode haver mais do que um componente do mesmo tipo dentro e com estilo diferente, usando essa técnica não seria possível pegar o seletor.

### Componentes longos

Esses dependem de um pré processamento, como SASS, Stylus ou LESS. Constituem de componentes que herdam características de outros.

```scss
// SCSS pré definido
.Heading3 {
    ...
}

.Paragraph {
    ...
}
```

```html
<!-- HTML do tooltip -->
<div class="Tooltip">
    <h3 class="Tooltip-title">TITLE</h3>
    <p class="Tolltip-message">message</p>
</div>
```

```scss
// SCSS do tooltip com possíveis sobrescritas
.Tooltip {
    ...
}

.Tooltip-title {
    @extend .Heading3;
    ...
}

.Tooltip-message {
    @extend .Paragraph;
    ...
}
```

#### Vantagens:

- semântico
- organização do CSS pré processado.

#### Desvantagens:

- sempre precisa usar o @extend (aumenta o trabalho).
- gera mais código.

### Componentes mistos

```scss
// CSS pré definido
.Heading3 {
    ...
}

.Paragraph {
    ...
}
```

```html
<!-- HTML do tooltip -->
<div class="Tooltip">
    <h3 class="Tooltip-title Heading3">TITLE</h3>
    <p class="Tolltip-message Paragraph">message</p>
</div>
```

```scss
// CSS do tooltip com possíveis sobrescritas
.Tooltip {
    ...
}

.Tooltip-title {
    ...
}

.Tooltip-message {
    ...
}
```

#### Vantagens:

- semântico
- prático

#### Desvantagens:

- mais código HTML

**Obs:** as formas mecionadas acima são componentes dentro de outros, nenhuma está utilizando utilitários CSS como ```.u-textCenter```, ```.u-clearFix```.


